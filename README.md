# TP Analyse des données RNA-seq

Exécution des étapes d'alignement et comptages sur le cluster de calcul pedago-ngs.
L'analyse d'expression différentielle est réalisé sur local, import des fichiers de comptages avec protocol scp. 
### 0. definition des variables bash:
```
dirprof=/data/share/MADT/TP-VincentLacroix
STAR=/data/share/MADT/TP-VincentLacroix/STAR-2.6.1d/bin/Linux_x86_64/STAR
dirindex=/newdisk/STARIndex_GRCh38_Gencode28/
OUTPUT=~/tpRNAseq
```
note:
/newdisk/STARIndex/hg38Gencode22StarIndex/labshare.cshl.edu/shares/gingeraslab/www-data/dobin/STAR/STARgenomes/GENCODE/GRCh38_Gencode22
avait ete sugeré mais la version 22 est plus ancienne que v.28

##### arborescence des fichiers fastq:
```
tree $dirprof/fastq
/data/share/MADT/TP-VincentLacroix/fastq
├── siCTL_R1
│   ├── SRR5221953_1.fastq
│   ├── SRR5221953_1_10M.fastq
│   ├── SRR5221953_2.fastq
│   ├── SRR5221953_2_10M.fastq
│   └── nohup.out
├── siCTL_R2
│   ├── SRR5221954_1.fastq
│   ├── SRR5221954_1_10M.fastq
│   ├── SRR5221954_2.fastq
│   ├── SRR5221954_2_10M.fastq
│   └── nohup.out
├── siDDX5_17_R1
│   ├── SRR5221955_1.fastq
│   ├── SRR5221955_1_10M.fastq
│   ├── SRR5221955_2.fastq
│   ├── SRR5221955_2_10M.fastq
│   └── nohup.out
└── siDDX5_17_R2
    ├── SRR5221956_1.fastq
    ├── SRR5221956_1_10M.fastq
    ├── SRR5221956_2.fastq
    ├── SRR5221956_2_10M.fastq
    └── nohup.out
```
## 1. Alignement (exemple commande):
```
$STAR --runThreadN 4 --genomeDir $dirindex --readFilesIn $dirprof/fastq/siDDX5_17_R1/SRR5221955_1_10M.fastq \
$dirprof/fastq/siDDX5_17_R1/SRR5221955_2_10M.fastq --outFileNamePrefix $OUTPUT/siDDX5_17_R1
```
## 2. Comptages: HTseq-Count
localisation de l'outil : 
/data/home/vincent/miniconda3/bin/htseq-count

demander de l'aide :
/data/home/vincent/miniconda3/bin/htseq-count --help

## 3. Lancement de HTSeq-count sur les fichiers SAM:

1. sort sam files (must be done before) (docu: http://www.htslib.org/doc/samtools-sort.1.html):

For exemple: 
cd tpRNAseq
samtools sort siDDX5_17_R1Aligned.out.sam --output-fmt SAM > siDDX5_17_R1Aligned.sorted.sam

Much better with loop:
```
for i in $(ls *Aligned.out.sam); do
   samtools sort $i --output-fmt SAM > ${i:: -8}.sorted.sam
done;
```
***By default, alignements are sorted by position***

2. Comme première messure, nous avons besoin du fichier GTF ayant la même 
nomenclature (européenne) des chromosomes que l'Index utilisé (étape d'alignement).

Exemple pour donner le format européen au fichier ayant noménclature américaine:
`sed 's/chr//' gencode.v33.primary_assembly.annotation.gtf > gencode33primary_eur.gtf`

3. Launch HTseq-count: 
mkdir counts 
/data/home/vincent/miniconda3/bin/htseq-count -r pos --stranded no siDDX5_17_R1Aligned.sorted.sam \
$MYANNOTATIONFILE > counts/mcfcellssiDDX5_17_R1.counts

Ou mieux avec une simple boucle bash (by creating a .sh executable file):

```
#!/bin/bash
mkdir counts
MYANNOTATIONFILE=gencode33primary_eur.gtf
directory for count files: `mkdir counts`
for i in $(ls *.sorted.sam);
do
    echo ${i:: -11}
    /data/home/vincent/miniconda3/bin/htseq-count -r pos --stranded no ${i} \
    $MYANNOTATIONFILE \
    > counts/mcfcells${i:: -11}.count;
done
```

### Option pour comptages: mmquant

HTseq-count par défault supprime les alignements non-uniques.
Si nous sommes interesés en inclure les multimapings, nous utilisons mmquant.
Mais ce n'est pas l'idéal pour un analyse d'expression differentielle, peut donner lieu à des faux positifs !!! (voir FAQ de HTseq-count sur son site)

Cet outil se trouve dans https://bitbucket.org/mzytnicki/multi-mapping-counter/src/master/

dans le terminal:
```
cd tpRNAseq
git clone https://bitbucket.org/mzytnicki/multi-mapping-counter.git
cd /multi-mapping-counter 
make
```
make has built the executable, to get help we type:
```
./mmquant -h
```
applying to sam files:
```
PX=/data/home/dgalvis/tpRNAseq
ANNOTATION=gencode33primary_eur.gtf
./mmquant -o $PX/res_mmquant/mmquant.counts -n CTL_R1 CTL_R2 DDX5_17_R1 DDX5_17_R2 -s U -f SAM -t 4 -a $ANNOTATION -r $PX/siCTL_R1_Aligned.sorted.sam $PX/siCTL_R2_Aligned.sorted.sam $PX/siDDX5_17_R1Aligned.sorted.sam $PX/siDDX5_17_R2Aligned.sorted.sam
```
options :
* by default : 
  - -s strand unknown U
  - -e  is "y" and that is precisely the case: reads are sorted (by position).
  - -l (number overlap type) is -1
 
The count table is loaded in DESeq2 by using for instance, `DESeqDataSetFromMatrix` function. 

## 4. DESeq2 

Le code complet pour l'execution d'une analyse d'expression differentielle 
sur les fichiers de comptages ici disponible se trouve dans 
`script_deseq2.R`, dans ce répertoire. 


## 5. DEXSeq
On utilisera le même fichier GTF de l'étape DESeq2. 

Testing conda's python3, only there it is possible to import HTSeq when working in *pedago-ngs*:

```
/data/home/vincent/miniconda3/bin/python3
Python 3.6.3 |Anaconda, Inc.| (default, Nov 20 2017, 20:41:42) 
[GCC 7.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import HTSeq
>>> exit()
```

I downloaded DEXSeq source files (only to have access to .py scripts)
wget https://bioconductor.org/packages/release/bioc/src/contrib/DEXSeq_1.32.0.tar.gz
tar -xvzf DEXSeq_1.32.0.tar.gz 
scripts location: `DEXSeq/inst/python_scripts/` into my workingdir (tpRNAseq)

#### Prepare annotation & dexseq_count 

Make sure that your current working directory contains the GTF file when preparing GFF:

```
/data/home/vincent/miniconda3/bin/python3 DEXSeq/inst/python_scripts/dexseq_prepare_annotation.py gencode33primary_eur.gtf gencode33.gff
mkdir countsfordex
for i in $(ls *.sorted.sam);
do
    echo ${i:: -11}
    /data/home/vincent/miniconda3/bin/python3 DEXSeq/inst/python_scripts/dexseq_count.py -p yes -r pos -s no gencode33.gff ${i} countsfordex/mcfcells${i:: -11}.dextxt;
done
```
- options utilisés:
    * -p yes : paired-end yes
    * -r pos : sam sorted by position, explained in section 3.
    * -s no : no strand-specific experiment

Les fichiers de comptages .txt sont alors chargés en R pour les analyses avec le package **DEXSeq**
Voir script `script_dexseq.R` dans le répertoire `DEXseq`.


# 6. Assemblage *DE NOVO*
Nous utilisons kissplice: voir le répertoire ` denovo_kissplice ` contenant les scripts et les résultats.

*******
Other Helping commands:
- sum counts column (bash):
    awk '{sum+=$2} END {print sum}' mcfcellssiDDX5_17_R2Aligned.dextxt

*******
sources:
- https://ss64.com/bash/screen.html
- https://www.biostars.org/p/343138/
- https://htseq.readthedocs.io/en/release_0.11.1/count.html#counting-reads-in-features-with-htseq-count
- https://bioconductor.org/packages/release/bioc/html/DESeq2.html
- https://bioconductor.org/packages/release/bioc/vignettes/DEXSeq/inst/doc/DEXSeq.html
- http://www.bioconductor.org/packages/release/bioc/manuals/AnnotationDbi/man/AnnotationDbi.pdf
- http://kissplice.prabi.fr/documentation/
