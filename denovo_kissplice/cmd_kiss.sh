#!/bin/bash
# 04 03 2020 Johanna

STARLo=/data/share/MADT/TP-VincentLacroix/STAR-2.6.1d/bin/Linux_x86_64/STARlong
dirindex=/newdisk/STARIndex_GRCh38_Gencode28
outdir=kissresults
tmpdir=kisstmp

cd datatrimkiss

echo $f1

if [ -d $outdir ]; then
        rm -r $outdir
fi

if [ -d  $tmpdir ]; then
        rm -r $tmpdir
fi
mkdir $outdir
mkdir $tmpdir

echo "kissplice on trimmed reads from 10M samples: "
unlimit -s unlimited
echo "stack size increased to 'unlimited'"
~/tpRNAseq/kissplice-2.4.0-p1/bin/kissplice -r CTL_R1.1.trimmed.fastq -r CTL_R1.2.trimmed.fastq \
-r CTL_R2.1.trimmed.fastq -r CTL_R2.2.trimmed.fastq -r DDX5_17_R1.1.trimmed.fastq -r DDX5_17_R1.2.trimmed.fastq$
-r DDX5_17_R2.1.trimmed.fastq -r DDX5_17_R2.2.trimmed.fastq -o $outdir -d $tmpdir -v


echo "now, its time fooooor..... mapping post-assembly"
if [ -f $outdir/*coherents_type_1.fa ]; then
        $STARLo --runThreadN 4 --genomeDir $dirindex --readFilesIn $outdir/*coherents_type_1.fa \
        --outFileNamePrefix $outdir/map_type_1
else
        echo "check pipeline and paths, no ${outdir}/*coherents_type_1.fa file exists"
        exit 1
fi
echo "running kissplice2refgenome, in this location: "$outdir
cd $outdir
/data/share/MADT/TP-VincentLacroix/kissplice2refgenome-1.2.3/kissplice2refgenome \
-a ~/tpRNAseq/v33.gff --pairedEnd True map_type_1Aligned.out.sam

echo "your file 'events.txt' contains the results, check for errors if not"
#end

